## Installation

Run ```docker-compose up -d``` from folder root to run the application.

You can view the app at ```http://127.0.0.1:8009/index``` .

Data Grid is at ```http://127.0.0.1:8009/index```.

Form with Files is at ```http://127.0.0.1:8009/create```

## Mysql 10,000 rows at one sec

```mysql -u username -p database_name < file.sql```
I think mysql direct command is fastest way to insert large data set very fast.


```INSERT INTO tbl_name (a,b,c) VALUES(1,2,3),(4,5,6),(7,8,9);``` 
Inserting multiple values in mysql may insert fast.
 


 

