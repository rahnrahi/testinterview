<?php
include 'dbsconfig.php';

if (isset($_POST['draw'])) {

  $query = "SELECT * from testcontent where 1 ";


  $limit    = $_POST['length'];
  $offset   = $_POST['start'];
  $columns  = $_POST['columns'];
  $order  = $_POST['order'];
  $search = $_POST['search']['value'];

  $filterQuery = ""; 

  if (isset($search) && !empty($search)) {
    $filterQuery = " and (`author` like '%{$search}%' or `country` like '%{$search}%') ";
  }

  $allGroupsCount = $dbh->query('select count(*) from testcontent')->fetchColumn();
  $resultDatatable['recordsTotal'] = $allGroupsCount;
  $resultDatatable['recordsFiltered'] = $dbh->query("select count(*) from testcontent where 1 {$filterQuery}")->fetchColumn();

  $query .= $filterQuery; 

  //$allGroupsQuery->limit($limit)->offset($offset);

  foreach ($order as $rd) {
    $clmIndex = (int) $rd['column'];
    $clmName = $columns[$clmIndex]['data'];
    $dir = $rd['dir'];
    $query .= " order by {$clmName} {$dir}";
  }
  $query .= " LIMIT {$limit} OFFSET {$offset}";
  $allResults = array();
  foreach ($dbh->query($query)->fetchAll(PDO::FETCH_ASSOC) as $row) {
    $allResults[] = $row;
  }

  //getListFormattedData($allResults);
  $resultDatatable['draw'] = $_POST['draw'];
  $resultDatatable['data'] = $allResults;
  echo json_encode($resultDatatable);
  exit;
}

include './views/list.php';
