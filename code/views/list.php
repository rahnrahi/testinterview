<!DOCTYPE html>
<html>

<head>
  <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css">
  <script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>
</head>

<body>

  <h1>This Data grid List</h1>
  <a href="/create">Create New</a>
  <table id="allTable" class="table table-striped"></table>
  <script>
    $(function() {
      allGroupsTable = $('#allTable').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: '',
          type: 'POST',
          dataSrc: function(json) {
            tableLocalData = json.data;
            return json.data;
          }
        },

        columnDefs: [
          {
            targets: 0,
            title: 'ID',
            data: 'id'
          },
          {
            targets: 1,
            title: 'Author Name',
            data: 'author'
          },
          {
            targets: 2,
            orderable: true,
            title: 'Country',
            data: 'country'
          },
          {
            targets: 3,
            orderable: false,
            title: 'Picture JPG',
            data: 'picone'
          },
          {
            targets: 4,
            orderable: false,
            title: 'Picture PNG',
            data: 'pictwo'
          }
        ]
      });
    })
  </script>
</body>

</html>