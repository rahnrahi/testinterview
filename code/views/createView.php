<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js" ></script>
<!------ Include the above in your HEAD tag ---------->

<div class="container">
   <h1>Form Data Post With Files</h1>
   <hr/>
            <form class="form-horizontal" id="testForm" role="form">
               
                <div class="form-group">
                    <label for="firstName" class="col-sm-3 control-label">Author Name</label>
                    <div class="col-sm-9">
                        <input type="text" id="author" name="author" placeholder="Author Name" class="form-control required" autofocus>
                        <span class="help-block">Last Name, First Name, eg.: Smith, Harry</span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="country" class="col-sm-3 control-label">Country</label>
                    <div class="col-sm-9">
                        <select id="country" name="country" class="form-control required">
                            <option>Afghanistan</option>
                            <option>Bahamas</option>
                            <option>Cambodia</option>
                            <option>Denmark</option>
                            <option>Ecuador</option>
                            <option>Fiji</option>
                            <option>Gabon</option>
                            <option>Haiti</option>
                        </select>
                    </div>
                </div> <!-- /.form-group -->
            
                <div class="form-group">
                    <label for="country" class="col-sm-3 control-label">Upload JPG file</label>
                    <div class="col-sm-9 col-sm-offset-3">
                        <div class="checkbox">
                            <label>
                              <input type="file" required name="picjpg" accept="image/*">
                            </label>
                        </div>
                    </div>
                </div> <!-- /.form-group -->

                <div class="form-group">
                   <label for="country" class="col-sm-3 control-label">Upload PNG file</label>
                    <div class="col-sm-9 col-sm-offset-3">
                        <div class="checkbox">
                            <label>
                              <input type="file" required name="picpng" accept="image/*">
                            </label>
                        </div>
                    </div>
                </div> <!-- /.form-group -->

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button type="button" id="save_form"  class="btn btn-primary btn-block">Save</button>
                    </div>
                </div>
                
            </form> <!-- /form -->
        </div> <!-- ./container -->
        <prev id="responseView"></prev>
        <script src="../assets/form.js" ></script>
        <style>
          .error {
            color: red
          }
        </style>