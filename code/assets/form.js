$(function(){
  $("#save_form").click(function(){
    if($("#testForm").valid()){
      // Get form
      var form = $('#testForm')[0];
      // Create an FormData object 
      var data = new FormData(form);
      data.append('otherdata', $('#testForm').serialize());


      $.ajax({
          type: "POST",
          enctype: 'multipart/form-data',
          url: "",
          data: data,
          processData: false,
          contentType: false,
          cache: false,
          timeout: 600000,
          success: function (data) {

              $("#responseView").html(data);
              console.log("SUCCESS : ", data);

          },
          error: function (e) {

              $("#responseView").html(e.responseText);
              console.log("ERROR : ", e);

          }
      });
    }
  })
})