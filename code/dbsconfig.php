<?php
$servername = "localhost";
$username = "root";
$password = "secret";

try {
  $dbh = new PDO('mysql:host=localdbtest;dbname=interviewtest', $username, $password);
} catch (PDOException $e) {
  print "Error!: " . $e->getMessage() . "<br/>";
  die();
}